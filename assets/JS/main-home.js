// //////////////////////////////////////////////////////

var cartID = 1;
function loadJson() {
    fetch("db.json")
        .then((response) => response.json())
        .then((data) => {
            var getListCourses = data.listCourses;
            var html = "";
            getListCourses.map((getData) => {
                var lengthTxt = getData.para.length;
                var coverList = document.querySelector("#cover-list");
                if (lengthTxt >= 120) {
                    var theRest;
                    theRest = getData.para.slice(0, 300) + "...";
                }

                html += ` <div class="wrapper">
                <div class="col-lg-4 col-md-5 col-12 big-image">
                     <img src=${getData.src}> 
                </div>
                <div class="col-lg-8 col-md-7 col-12">
                    <a href="./detailsCourses.html" id="content-blog">
                         <h3>${getData.title}</h3>
                    </a>
                    <div class="inner-content-blog">
                        <div class="inner">
                            <img src="${getData.price}"><p class="cost-course">${getData.cost}</p>
                            <img src="${getData.users}"><p class="student-use">${getData.studentUse} <span>students enrolled</span></p>
                            <img src="${getData.rating}"/>
                        </div>
                             <p class="para">${theRest}</p>
                        <a href="#" class="cart">
                             <button class="addToCart" type="button"> add to cart </button>
                        </a>
                    </div>
                    
                    </div>
                </div>`;

                coverList.innerHTML = html;
            });
        });
    fetch("db.json")
        .then((response) => response.json())
        .then((data) => {
            var html = "";
            var coverSt = document.querySelector("#cover-st");
            data.listStudents.map((getValue) => {
                var lengthTxt = getValue.para.length;
                if (lengthTxt >= 120) {
                    var theRest;
                    theRest = getValue.para.slice(0, 120) + "...";
                }
                html += ` <div class="col-xl-4 col-md-6 col-12 list-feedback">
                                     <div class="avatar">
                                    <div>
                                        <img src=${getValue.avatar} alt="" class="style-avatar">
                                        <a href="#">
                                            <h5>${getValue.name}</h5>
                                            <p>${getValue.address}</p>
                                        </a>
                                    </div>
                                    <div class="message">
                                        <a href="#" >
                                            <img src=${getValue.src} alt="">
                                            <p class="content">${theRest}</p>
                                        </a>
                                    </div>
                                </div>
                           </div> `;

                coverSt.innerHTML = html;
            });
        });
    fetch("db.json")
        .then((response) => response.json())
        .then((data) => {
            var html = "";
            data.lastestNews.map((value) => {
                var lastNews = document.getElementById("lastest-news");

                if (lastNews) {
                    lastNews.classList.remove("list-feedback");
                }
                html += `
                <div class="col-xl-6 col-lg-4 col-md-6 col-12">
                <div class="pic">
                    <img class="img-left" src=${value.src} alt="">
                    <div class="bg-blue"><img src=${value.src2} alt="">
                    <div class="text-describe">
                    <p>${value.para}</p>
                    <div class="cover">
                        <div class="btn-read">
                        <a class="inner" href=""><img src=${value.button} alt=""></a>
                        </div>
                        <div class="btn-share">
                        <a class="share" href=""><img src=${value.button2} alt=""></a>
                        </div>
                    </div>
                    </div>
                    </div>
                            </div>
                        </div> `;

                lastNews.innerHTML = html;
            });
        });
    fetch("db.json")
        .then((response) => response.json())
        .then((data) => {
            var html = "";
            data.recentPosts.map((value) => {
                html += `
                    <img src=${value.src} alt="">
                    <a href="#" class="content-post">
                        <p>${value.title}</p>
                        <span>${value.day}</span>
                    </a>`;
            });
            document.getElementById("detail-posts").innerHTML = html;
        });
}

eventListeners();
function eventListeners() {
    window.addEventListener("DOMContentLoaded", () => {
        loadJson();
        loadCart();
    });
}

/////////////////////////////////////////////////////////////////////////////
function handleCourse() {
    var indexCart = document.querySelector(".index-cart");
    var coverList = document.querySelector("#cover-list");
    coverList.addEventListener("click", getClickCourse);
    var effectButton = document.querySelector(".buttoncart button i");
    effectButton.addEventListener("click", function () {
        // indexCart.style.display = "block";

        if ((indexCart.style.display = "none")) {
            indexCart.style.display = "block";
        }
        // indexCart.classList.add("visible");
        var closeCart = document.querySelector(".top-cart i");
        if (closeCart) {
            closeCart.addEventListener("click", handleCart);
            function handleCart() {
                var getElement = closeCart.parentElement.parentElement;
                if (getElement) {
                    getElement.style.display = "none";
                }
            }
        }
    });
    indexCart.addEventListener("click", deleteItemCart);
    console.log(indexCart);
}
handleCourse();
function updateCartInfor() {
    let cartInfo = findCartInfo();
    let totalCost = document.querySelector(".number");
    let cartTotalValue = document.querySelector("#totalValue");
    let costMoney = document.querySelector("#money");
    // costMoney.innerHTML = cartInfo.total + " $";

    totalCost.textContent = cartInfo.total + " $";
    cartTotalValue.textContent = cartInfo.productCount;
}
updateCartInfor();
function getClickCourse(e) {
    e.preventDefault();
    var getClass = e.target.classList.contains("addToCart");
    if (getClass) {
        var product =
            e.target.parentElement.parentElement.parentElement.parentElement;
        getCourse(product);
    }
}

function getCourse(product) {
    var courseInfo = {
        id: cartID,
        title: product.querySelector("#content-blog h3").textContent,
        cost: product.querySelector(".cost-course").textContent,
        studentUse: product.querySelector(".student-use").textContent,
        image: product.querySelector(".big-image img").src,
    };
    cartID++;
    addToListCart(courseInfo);
    saveProductInLocal(courseInfo);
}

function addToListCart(product) {
    var cartItem = document.createElement("div");
    cartItem.classList.add("detail-cart");
    cartItem.setAttribute("data-id", `${product.id}`);
    var lengthTxt = product.title.length;

    cartItem.innerHTML = `
                    <div class="product row">
                        <div class="col-md-3">
                            <img src="${product.image}" />
                        </div>
                        <div class="name-product col-md-6">
                            <h3 class="title">${
                                lengthTxt > 23
                                    ? product.title.slice(0, 20) + "..."
                                    : product.title
                            }</h3>
                            <!-- <p class="detail"> asudifjnm2fohjk.m,evuijkfv</p> -->
                            <p class="number-of-student">${
                                product.studentUse
                            }</p>
                        </div>
                        <div class="cost-product col-md-2">
                                    <p>${product.cost}<span>$</span></p>
                        </div>
                        <div class="col-md-1"><i class="delete fa fa-trash" aria-hidden="true"></i>
                        </div>
                    </div>
                  
                    `;

    document.querySelector(".name").appendChild(cartItem);
}

//save product in local storage.

function saveProductInLocal(item) {
    let product = getProductFromLocal();

    product.push(item);
    localStorage.setItem("products", JSON.stringify(product));
    updateCartInfor();
}

function getProductFromLocal() {
    return localStorage.getItem("products")
        ? JSON.parse(localStorage.getItem("products"))
        : [];
}
function loadCart() {
    let products = getProductFromLocal();

    if (products.length < 1) {
        cartID = 1;
    } else {
        cartID = products[products.length - 1].id;
        cartID++;
    }
    products.forEach((product) => addToListCart(product));
}

function findCartInfo() {
    let products = getProductFromLocal();

    let total = products.reduce((acc, product) => {
        let price = parseFloat(product.cost);
        return (acc += price);
    }, 0);

    return {
        total: total,
        productCount: products.length,
    };
}

function deleteItemCart(e) {
    let cartItem;
    if (e.target.tagName === "BUTTON") {
        cartItem = e.target.parentElement;
        cartItem.remove();
    } else if (e.target.tagName === "I") {
        cartItem = e.target.parentElement.parentElement.parentElement;
        cartItem.remove();
    }
    let products = getProductFromLocal();
    let updateProducts = products.filter((product) => {
        return product.id !== parseInt(cartItem.dataset.id);
    });
    localStorage.setItem("products", JSON.stringify(updateProducts));

    updateCartInfor();
}

{
    /* <div class="col-md-2">
<input class="cart-quantity-input" type="number" value="${
    product.id
}">
</div>  */
}
// //////////////////////////////////////////////////////////////////

var navbar = document.querySelector(".wrapped");
var sticky = navbar.offsetTop;
window.onscroll = function () {
    My();
};

function My() {
    if (window.pageYOffset >= sticky) {
        navbar.classList.add("sticky");
        // navbar.classList.remove("container");
    } else {
        navbar.classList.remove("sticky");
    }
}
////////////////////////////////////////////////////////////////////////

var addCart = document.querySelectorAll("#addToCart");

////////////////////////////////////////////////////////////////////////////////////////////////
// var recentPost = "http://localhost:3000/recentPosts";

//

var courses = [
    {
        id: guidGenerator(),
        name: "Hoang hon thang 8 em oi",
        amount: 0,
        cost: 12,
    },
    {
        id: guidGenerator(),
        name: "Hoang hon thang 8 em oi",
        amount: 0,
        cost: 12,
    },
    {
        id: guidGenerator(),
        name: "Hoang hon thang 8 em oi",
        amount: 0,
        cost: 12,
    },
    {
        id: guidGenerator(),
        name: "Hoang hon thang 8 em oi",
        amount: 0,
        cost: 12,
    },
    {
        id: guidGenerator(),
        name: "Hoang hon thang 8 em oi",
        amount: 0,
        cost: 12,
    },
];
function guidGenerator() {
    var S4 = function () {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    };
    return (
        S4() +
        S4() +
        "-" +
        S4() +
        "-" +
        S4() +
        "-" +
        S4() +
        "-" +
        S4() +
        S4() +
        S4()
    );
}
// function handleCourses() {
//     courses.map((data) => {
//         var getAmount = document.getElementById("amount-count");
//         var subtraction = document.getElementById("subtraction");
//         var sum = document.getElementById("sum");
//         var totalMoney = document.getElementById("cost");
//         var number = document.getElementById("number");
//         var product = document.getElementById("money");
//         var provisional = document.getElementById("total-money");
//         subtraction.onclick = function () {
//             if (data.amount <= 0) {
//                 getAmount.innerHTML = 0;
//             } else {
//                 getAmount.innerHTML = --data.amount;
//                 number.innerHTML = getAmount.innerHTML;
//                 totalMoney.innerText = data.cost * data.amount;
//                 product.innerText = data.amount * data.cost;
//             }
//         };
//         sum.onclick = function () {
//             getAmount.innerHTML = ++data.amount;
//             number.innerHTML = getAmount.innerHTML;
//             totalMoney.innerHTML = data.cost * data.amount;
//             product.innerText = data.amount * data.cost;
//         };
//     });
// }

// handleCourses();

//

//
// data.listBlog.map((getData) => {
//     var coverListBlog = document.querySelector("#list-blog");
//     html += `
//         <div class="wrapper">
//             <div class="col-xl-4 col-md-5 col-12">
//                 <a href="./detailsBlog.html" id="blog-img">
//                      <img src=${getData.src} alt="">
//                 </a>
//             </div>
//             <div class="col-xl-8 col-md-7 col-12">
//                     <div id="content-blog">
//                     <a href="./detailsBlog.html">
//                      <h3>${getData.title}</h3>
//                      </a>
//                  <div class="inner-content-blog">
//                 <div class="inner">
//                         <p class="first">${getData.own}</p>
//                         <p class="comment">${getData.comment}</p>
//                         <p class="date">${getData.date}</p>
//                 </div>
//                         <p class="para">${getData.para}</p>
//                         <a href="./detailsBlog.html">
//                     <button type="submit">${getData.button}</button>
//                     </a>
//                  </div>
//                     </div>
//                 </div>
//         </div>`;

//     return (coverListBlog.innerHTML = html);
// });
